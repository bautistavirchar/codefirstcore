using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace codeFirstCore.Data
{
    public class CodeFirstDbContext : DbContext
    {
        public virtual DbSet<Employees> Employees { get; set; }
        public virtual DbSet<Departments> Departments { get; set; }
        public CodeFirstDbContext(DbContextOptions<CodeFirstDbContext> options) 
        : base(options){ }

        public CodeFirstDbContext(){}
    }



    public class Employees{
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime Birthdate { get; set; }
        public Departments Departments { get; set; }
    }

    public class Departments{
        public Departments()
        {
            Employees = new HashSet<Employees>();
        }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ICollection<Employees> Employees{get;set;}
    }
}