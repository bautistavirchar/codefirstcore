using System.Linq;
using codeFirstCore.Data;

namespace codeFirstCore.Services
{
    public class RepoEmployees
    {
        public object GetResult(){
            using(var db = new CodeFirstDbContext()){
                var rows = db.Employees.Select(x => new{
                    Id = x.Id,
                    Name = x.Name,
                    Department = x.Departments.Name
                });
                return rows;
            }
        }
    }
}